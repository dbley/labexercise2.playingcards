#include <iostream>
#include <conio.h>

// Card suit enumeration.
enum Suit
{
	CLUBS,
	DIAMONDS,
	HEARTS,
	SPADES
};

// Card rank enumeration.
enum Rank
{
	ONE = 1,
	TWO = 2,
	THREE = 3,
	FOUR = 4,
	FIVE = 5,
	SIX = 6,
	SEVEN = 7,
	EIGHT = 8,
	NINE = 9,
	TEN = 10,
	JACK = 11,
	QUEEN = 12,
	KING = 13,
	ACE = 14
};

// Card struct.
struct Card
{
	Suit suit;
	Rank rank;
};


int main()
{


	

	_getch();
	return 0;
}
